var sequelize = require("sequelize")
var models = require("../models");
var mstGoods = models.mst_goods;
var mstGoodsCateg = models.mst_goods_categ;
var mstGoodsPrice = models.mst_goods_price;
var mstGoodsUom = models.mst_goods_uom;


exports.add = (req,res) => {
    console.log(req.body.nama);
    // sequelize.Transaction().then(function(a){
    //     mstGoods.create({
    //         category_id: req.body.kategori,
    //         uom_id: req.body.uom,
    //         name: req.body.nama},
    //         {
    //             transaction: a
    //     }).then(function(){
    //         a.commit()
    //         res,status(200).json({
    //             message: "berhasil",
    //             status: 200
    //         })
    //     }).catch(function(error) {
    //         console.log(error);
    //         a.rollback();
    //     })
    // })
    return models.sequelize
          .transaction((rollback) => {
            return models.mst_goods
              .create(
                {
                    category_id: req.body.kategori,
                    uom_id: req.body.uom,
                    name: req.body.nama,
                }
                ,{
                  transaction: rollback,
                }
              )
              .then((data) => {
                  console.log(data)
                return data;
              })
              .catch((err) => {
                  throw err;
                // throw {
                //   name: "Custom",
                //   message: {
                //     eng: "Oops! Failed to checkout, please retry.",
                //     ind: "Oops! Gagal checkout, mohon coba lagi.",
                //   },
                // };
              });
          }).then(()=>{
            res.status(200).json({
                message: "data berhasil ditambahkan",
                status: 200,
            });
          })
    
}
exports.goods= (req,res) =>{
    let promise = []
    if(req.method === "GET"){
        mstGoods.findAll({
            include: [
                {
                    model: mstGoodsCateg,
                }, 
                {
                    model: mstGoodsUom,
                }
            ]
        })
        .then((goods)=>{
            if (goods.length>0){
                let arr = [];
                goods.map(function(item){
                    arr.push({
                        id: item.id,
                        namaBarang: item.name,
                        satuan: item.mst_goods_uom.name,
                        kategori: item.mst_goods_categ.name
                    });
                });
                res.status(200).json({
                    message: "berhasil!",
                    status: 200,
                    data: arr,
                });
            } else {
                throw{
                    name: "Custom",
                    message: "gagal"
                }
            }
        })
    } else if (req.method === "POST"){
        if (req.body.goods_id != ""){
            mstGoods.update({
                name: req.body.nama,
                uom_id: req.body.uom,
                category_id: req.body.kategori
            },{
                where:{
                    id:req.body.goodsId
                }
            }).then((data)=>{
                console.log(data)
            }
            ).catch((err)=>{
                console.log(err)
            }).then(()=>{
                res.status(200).json({
                    message: "data berhasil diubah",
                    status: 200,
                });
              })
        }
        else{
            res.status(200).json({
                message: "masukan goodsId",
                status: 400
            })
        }
    }
}