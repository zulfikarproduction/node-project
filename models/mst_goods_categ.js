/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mst_goods_categ', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'mst_goods_categ',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "mst_goods_categ_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
