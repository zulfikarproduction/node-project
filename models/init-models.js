var DataTypes = require("sequelize").DataTypes;
var _mst_goods = require("./mst_goods");
var _mst_goods_categ = require("./mst_goods_categ");
var _mst_goods_price = require("./mst_goods_price");
var _mst_goods_uom = require("./mst_goods_uom");

function initModels(sequelize) {
  var mst_goods = _mst_goods(sequelize, DataTypes);
  var mst_goods_categ = _mst_goods_categ(sequelize, DataTypes);
  var mst_goods_price = _mst_goods_price(sequelize, DataTypes);
  var mst_goods_uom = _mst_goods_uom(sequelize, DataTypes);


  return {
    mst_goods,
    mst_goods_categ,
    mst_goods_price,
    mst_goods_uom,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
