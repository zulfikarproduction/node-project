/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
    const goods = sequelize.define('mst_goods', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    category_id: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    uom_id: {
      type: DataTypes.INTEGER(2),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(120),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'mst_goods',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "mst_goods_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  goods.associate = function(models) {
    goods.belongsTo(models.mst_goods_categ , {
      foreignKey: "category_id",
      sourceKey: "id"});
    goods.belongsTo(models.mst_goods_uom , {
      foreignKey: "uom_id",
      sourceKey: "id"
    })
  }
  return goods;
};
