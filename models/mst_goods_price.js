/* jshint indent: 2 */

const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mst_goods_price', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    po_price: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    dc_price: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'mst_goods_price',
    schema: 'public',
    timestamps: false,
    indexes: [
      {
        name: "mst_goods_price_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
